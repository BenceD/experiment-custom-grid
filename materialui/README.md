MaterialUI grid configuration

Need special tsconfig settings - https://material-ui.com/guides/typescript/

Since materialUI grid can't be separated from the components, we will need to make sure everyone avoid re-styling of materialUI compoents. It's not compatible with out design, and it will cause a lot of unexpected issues.

Notes:
- MaterialUI is designed with ROBOTO font in mind, so it's likely not compatible with our design, and font choice.
- Their components will be discarded with three shaking, so it have no impact to the bundle size.
