import { Container, createMuiTheme, CssBaseline, Grid, Paper, ThemeProvider, withStyles } from '@material-ui/core';
import React from 'react';
import './App.css';

function App() {
  const theme = createMuiTheme({
    breakpoints: {
      values: {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 1024,
        xl: 1440
      },
    },
  });

  const GlobalCss = withStyles({
    '@global': {
      'body': {
        backgroundColor: "#fff",
      },
      '.MuiContainer-root': {
        maxWidth: '1328px',
        paddingLeft: "24px",
        paddingRight: "24px",
        
        [theme.breakpoints.up('md')]: {
          paddingLeft: "56px",
          paddingRight: "56px",
        },
      },
    },
  })(() => null);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline /> {/* CSS RESET FOR CROSS-BROWSER COMPATIBILITY */}
      <GlobalCss /> {/* MUI GLOBAL OVERWRITE */}
      <Container maxWidth="xl">
        {/* ROUTER */}
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <div className="header"></div>
          </Grid>
        </Grid>
        
        <Grid container spacing={2}>
        {[...Array(4)].map(()=>(
          <Grid item sm={6} md={3}>
            <div className="content-image-placeholder"></div>
          </Grid>
        ))}
        </Grid>

        <Grid container spacing={2}>
        {[...Array(12)].map(()=>(
          <Grid item xs={1}>
            <div className="column"></div>
          </Grid>
        ))}
        </Grid>
      </Container>
    </ThemeProvider>
  );
}

export default App;
