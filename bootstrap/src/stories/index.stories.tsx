import {storiesOf} from '@storybook/react';
import {withKnobs} from '@storybook/addon-knobs';
import Button from './components/Button.story';

import '../App.scss';

storiesOf('Components', module)
  .addDecorator(withKnobs)
  .add('Button', Button);
