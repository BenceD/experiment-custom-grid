import { select } from '@storybook/addon-knobs';
import React from 'react';
import { Button } from '../../components/Button';


const ButtonStory = () => {
  return <Button primary={false} label="valami" />;
}

export default ButtonStory;