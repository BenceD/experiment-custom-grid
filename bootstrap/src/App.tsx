import React from 'react';
import logo from './assets/logo.svg';
import cart from './assets/cart.svg';
import search from './assets/search.svg';
import menu from './assets/menu.svg';
import './App.scss';

const Header = () => (
  <div className="header-container">
    <div className="container-fluid container-trt">
      <div className="row">
        <div className="col">
          <div className="header-inner">
            <div className="d-flex">
              <div className="d-flex d-md-none">
                <Icon src={menu}/>
              </div>
              <Logo/>
            </div>

            <div className="categories d-none d-sm-none d-md-flex">
              <NavButton title="Bistro"/>
              <NavButton title="Boutique"/>
              <NavButton title="Duty free"/>
              <NavButton title="Virtuals"/>
              <NavButton title="Charity"/>
              <NavButton title="Deals"/>
            </div>

            <div className="button-bar">
              <Icon src={cart} />
              <Icon src={search} />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

const NavButton = (props:any) => (
  <div className="nav-button">{props.title}</div>
)

const Icon = (props:any) => (
  <div className="button-icon">
    <img src={props.src} alt=""/>
  </div>
)

const Logo = ()=> (
  <img className="d-flex" src={logo} alt=""/>
)

const Content = () => (
  <div className="content">
    <div className="content-image-placeholder"></div>
    <p>Lorem ipsum</p>
  </div>
)

const Column = () => (
  <div className="column"></div>
)

function App() {
  return (
    <>
      <Header/>

      <div className="container-fluid container-trt">
        <div className="row">
          {[...Array(4)].map(()=>(
            <div className="col-sm-6 col-md-3 col-6">
              <Content/>
            </div>
          ))}
        </div>

        <div className="row">
          {[...Array(12)].map(()=>(
            <div className="col-1">
              <Column/>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

export default App;
